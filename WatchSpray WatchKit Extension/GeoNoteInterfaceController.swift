//
//  GeoNoteInterfaceController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 12.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import WatchKit
import CoreLocation
import WatchConnectivity

class GeoNoteInterfaceController: WKInterfaceController  {

    var session: WCSession?
    var locationManager: CLLocationManager = CLLocationManager()
    var mapLocation: CLLocationCoordinate2D?
    var note: String? {
        didSet {
            self.noteText.setText(note)
            self.noteText.setHidden(false)
            self.saveButton.setHidden(false)
            self.textInputButtonLabel.setText("Notiz ändern")
        }
    }

    // Array mit Vorschlägen
    let suggestions = [
        "Pfahl beschädigt",
        "Rebstock fehlt",
        "Draht beschädigt",
        "Pilzbefall",
        "Schädlingsbefall"
    ]

    @IBOutlet var mapView: WKInterfaceMap!
    @IBOutlet var noteText: WKInterfaceLabel!
    @IBOutlet var saveButton: WKInterfaceButton!
    @IBOutlet var textInputButton: WKInterfaceButton!
    @IBOutlet var textInputButtonLabel: WKInterfaceLabel!

    // Action, welche vom Button „Notiz eingeben“ aufgerufen wird
    @IBAction func textInput() {
        let sortedSuggestions = suggestions.sorted {
            $0.localizedCaseInsensitiveCompare($1) == .orderedAscending
        }
        presentTextInputController(withSuggestions: sortedSuggestions, allowedInputMode: .plain) { (results: [Any]?) -> Void in
            if results != nil {
                self.note = results!.first as? String
            }
        }
    }

    @IBAction func saveNote() {
        // send geo-note to iPhone
        session?.sendMessage(["geoNote": self.note!], replyHandler: nil) { (error) in
            NSLog("%@", "Error sending message: \(error)")
        }

        // pop to root controller
        popToRootController()
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)

        // Configure interface objects here.
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
        locationManager.requestLocation()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

        if WCSession.isSupported() {
            session = WCSession.default()
            session?.delegate = self
            session?.activate()
        }

        locationManager.startUpdatingLocation()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()

        locationManager.stopUpdatingLocation()
    }

}

extension GeoNoteInterfaceController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let currentLocation = locations[0]
        let lat = currentLocation.coordinate.latitude
        let long = currentLocation.coordinate.longitude

        self.mapLocation = CLLocationCoordinate2DMake(lat, long)

        let span = MKCoordinateSpanMake(0.001, 0.001)

        let region = MKCoordinateRegionMake(self.mapLocation!, span)
        self.mapView.setRegion(region)

        self.mapView.removeAllAnnotations()
        self.mapView.addAnnotation(self.mapLocation!, with: .red)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
}

extension GeoNoteInterfaceController: WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState: \(activationState) error: \(String(describing: error))")
    }
}
