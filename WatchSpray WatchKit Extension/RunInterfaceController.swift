//
//  RunInterfaceController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 16.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import WatchKit
import WatchConnectivity

class RunInterfaceController: WKInterfaceController {

    var session: WCSession?

    @IBOutlet var appContextLabel: WKInterfaceLabel!

    @IBAction func finishRun() {
        // pop this controller from screen
        pop()
        // send message to iPhone to inform about the finished run
        session?.sendMessage(["sprayRun": "Lauf beendet"], replyHandler: nil) { (error) in
            NSLog("%@", "Error sending message: \(error)")
        }
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

        if WCSession.isSupported() {
            session = WCSession.default()
            session?.delegate = self
            session?.activate()
        }
    }

    var messages = [String]() {
        didSet {
            OperationQueue.main.addOperation {
                self.updateAppContext()
            }
        }
    }

    func updateAppContext() {
        appContextLabel.setText(messages.last! + " %")
        if let recyclingRateValue = Double(messages.last!) {
            if recyclingRateValue > 30 {
                WKInterfaceDevice.current().play(.failure)
            }
        }
    }
}

extension RunInterfaceController: WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState: \(activationState) error: \(String(describing: error))")
    }

    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        self.messages.append(String(describing: applicationContext["recyclingRate"]!))
    }

}
