//
//  WorkSelectInterfaceController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 24.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import WatchKit

protocol WorkSelectorDelegate {
    func didSelectWork(_ work: String)
}

class WorkSelectInterfaceController: WKInterfaceController {

    var selectedWork: String?

    let workTitles = [
        "Ausbrechen",
        "Drahtarbeit",
        "Düngung",
        "Einsaat",
        "Entlauben (Hand)",
        "Entlauben (Maschine)",
        "Grubbern",
        "Handlese",
        "Jungfeldpflege",
        "Laubschnitt",
        "Mulchen",
        "Rebschnitt",
        "Rebholz entfernen",
        "Weinlese (Hand)",
        "Weinlese (Vollernter)",
        "Sonstiges"
    ]

    var delegate: WorktimeInterfaceController?

    @IBOutlet var workPicker: WKInterfacePicker!

    @IBAction func pickerDidChange(_ value: Int) {
        selectedWork = workTitles[value]
    }

    @IBAction func startWorktimeTracking() {
        self.dismiss()
        self.delegate?.didSelectWork(selectedWork!)
    }

    func setPickerItems() {
        let sortedWorkTitles = workTitles.sorted {
            $0.localizedCaseInsensitiveCompare($1) == .orderedAscending
        }
        workPicker.setItems(sortedWorkTitles.map { title in
            let item = WKPickerItem()
            item.title = title
            return item
        })
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)

        // Configure interface objects here.
        self.delegate = context as? WorktimeInterfaceController
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        setPickerItems()
        self.selectedWork = workTitles.first
    }

}
