 //
 //  WorktimeInterfaceController.swift
 //  WatchSpray
 //
 //  Created by Martin Eberle on 12.05.17.
 //  Copyright © 2017 Martin Eberle. All rights reserved.
 //

 import WatchKit
 import WatchConnectivity

 class WorktimeInterfaceController: WKInterfaceController, WorkSelectorDelegate {

    var session: WCSession?
    var timer: Timer?
    var timePassed = 0
    var timerIsPaused = false
    var selectedWork: String?
    var worktime: String?

    let standardUserDefaults = UserDefaults.standard

    @IBOutlet var timerDisplay: WKInterfaceTimer!
    @IBOutlet var timerStartPauseButton: WKInterfaceButton!
    @IBOutlet var timerFinishButton: WKInterfaceButton!

    @IBAction func startPauseTimer() {
        if timer != nil {
            pauseTimer()
        } else {
            startTimer()
        }
    }

    @IBAction func finishTimer() {
        stopTimer()
    }

    func didSelectWork(_ work: String) {
        self.selectedWork = work
        self.setTitle(self.selectedWork)
    }

    func startTimer() {
        self.setTitle(self.selectedWork)
        self.timerDisplay.setTextColor(UIColor.green)
        self.timerStartPauseButton.setTitle("Pause")
        self.timerIsPaused = false
        self.timerFinishButton.setHidden(false)
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.timePassed += 1
            let secondsPassed = Date(timeIntervalSinceNow: TimeInterval(self.timePassed))
            self.timerDisplay.setDate(secondsPassed)
        }
    }

    func pauseTimer() {
        self.timer?.invalidate()
        self.timer = nil
        self.timerIsPaused = true
        self.timerDisplay.setTextColor(UIColor.red)
        self.timerStartPauseButton.setTitle("Fortsetzen")
        self.timerFinishButton.setHidden(false)
    }

    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        self.worktime = String(describing: Date(timeIntervalSinceNow: TimeInterval(self.timePassed)))
        self.timePassed = 0
        self.timerIsPaused = false
        self.timerDisplay.setTextColor(UIColor.white)
        self.timerStartPauseButton.setTitle("Start")
        self.timerFinishButton.setHidden(true)
        self.timerDisplay.setDate(Date(timeIntervalSinceNow: TimeInterval(0)))

        // send geo-note to iPhone
        session?.sendMessage(["worktime": "\(self.selectedWork!): \(self.worktime!)"], replyHandler: nil) { (error) in
            NSLog("%@", "Error sending message: \(error)")
        }

        // pop to root controller
        popToRootController()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

        if WCSession.isSupported() {
            session = WCSession.default()
            session?.delegate = self
            session?.activate()
        }

        if standardUserDefaults.bool(forKey: "timerIsRunning") {
            self.selectedWork = standardUserDefaults.string(forKey: "selectedWork")
            if standardUserDefaults.bool(forKey: "timerIsPaused") {
                self.timePassed = standardUserDefaults.integer(forKey: "timePassed")
                let secondsPassed = Date(timeIntervalSinceNow: TimeInterval(self.timePassed))
                self.timerDisplay.setDate(secondsPassed)
                pauseTimer()
            } else {
                let timestamp = standardUserDefaults.integer(forKey: "timestamp")
                let timepassed = standardUserDefaults.integer(forKey: "timePassed")
                let startTime = timestamp - timepassed
                let now = Int(Date().timeIntervalSince1970)
                self.timePassed = now - startTime
                let secondsPassed = Date(timeIntervalSinceNow: TimeInterval(self.timePassed))
                self.timerDisplay.setDate(secondsPassed)
                startTimer()
            }
        } else if selectedWork == nil {
            presentController(withName: "WorkSelector", context: self)
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()

        if self.timer != nil || self.timerIsPaused {
            standardUserDefaults.set(true, forKey: "timerIsRunning")
            if self.timerIsPaused {
                standardUserDefaults.set(true, forKey: "timerIsPaused")
                self.timerIsPaused = false
            } else {
                standardUserDefaults.set(false, forKey: "timerIsPaused")
            }
            standardUserDefaults.set(Int(Date().timeIntervalSince1970), forKey: "timestamp")
            standardUserDefaults.set(Int(TimeInterval(self.timePassed)), forKey: "timePassed")
            standardUserDefaults.set(self.selectedWork, forKey: "selectedWork")
            standardUserDefaults.synchronize()
            if self.timer != nil {
                self.timer?.invalidate()
                self.timer = nil
            }
            self.timePassed = 0
        } else {
            standardUserDefaults.set(false, forKey: "timerIsRunning")
            standardUserDefaults.set(false, forKey: "timerIsPaused")
            standardUserDefaults.set(Int(TimeInterval(0)), forKey: "timePassed")
            standardUserDefaults.set(nil, forKey: "selectedWork")
            standardUserDefaults.synchronize()
        }
    }
    
 }

 extension WorktimeInterfaceController: WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState: \(activationState) error: \(String(describing: error))")
    }
 }
