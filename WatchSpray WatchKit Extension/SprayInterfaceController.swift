//
//  SprayInterfaceController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 22.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import WatchKit
import WatchConnectivity

class SprayInterfaceController: WKInterfaceController  {

    var locationManager: CLLocationManager = CLLocationManager()
    var mapLocation: CLLocationCoordinate2D?
    var session: WCSession?

    @IBOutlet var mapView: WKInterfaceMap!
    
    @IBAction func startRun() {
        // push to run controller
        pushController(withName: "Run", context: nil)

        // send message to iPhone to inform about the started run
        session?.sendMessage(["sprayRun": "Lauf gestartet"], replyHandler: nil) { (error) in
            NSLog("%@", "Error sending message: \(error)")
        }
    }

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)

        // Configure interface objects here.
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
        locationManager.requestLocation()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

        if WCSession.isSupported() {
            session = WCSession.default()
            session?.delegate = self
            session?.activate()
        }

        locationManager.startUpdatingLocation()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()

        locationManager.stopUpdatingLocation()
    }
}

extension SprayInterfaceController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let currentLocation = locations[0]
        let lat = currentLocation.coordinate.latitude
        let long = currentLocation.coordinate.longitude

        self.mapLocation = CLLocationCoordinate2DMake(lat, long)

        let span = MKCoordinateSpanMake(0.001, 0.001)

        let region = MKCoordinateRegionMake(self.mapLocation!, span)
        self.mapView.setRegion(region)

        self.mapView.removeAllAnnotations()
        self.mapView.addAnnotation(self.mapLocation!, with: .red)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension SprayInterfaceController: WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState: \(activationState) error: \(String(describing: error))")
    }
}
