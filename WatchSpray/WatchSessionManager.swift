//
//  WatchSessionManager.swift
//  WatchSpray
//
//  Created by Martin Eberle on 17.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import WatchConnectivity

class WatchSessionManager: NSObject, WCSessionDelegate  {

    var session = WCSession.default()

    dynamic var sprayRuns = [String]()
    dynamic var geoNotes = [String]()
    dynamic var worktimes = [String]()

    override init() {
        super.init()

        session.delegate = self
        session.activate()

        NSLog("%@", "Paired Watch: \(session.isPaired), Watch App installed: \(session.isWatchAppInstalled)")
    }

    // MARK: - WCSessionDelegate

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        if let sprayRun = message["sprayRun"] as? String {
            self.sprayRuns.append(sprayRun)
        } else if let geoNote = message["geoNote"] as? String {
            self.geoNotes.append(geoNote)
        } else if let worktime = message["worktime"] as? String {
            self.worktimes.append(worktime)
        }
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState: \(activationState), error: \(String(describing: error))")
    }

    func sessionDidBecomeInactive(_ session: WCSession) {
        NSLog("%@", "sessionDidBecomeInactive: \(session)")
    }

    func sessionDidDeactivate(_ session: WCSession) {
        NSLog("%@", "sessionDidDeactivate: \(session)")
    }

    func sessionWatchStateDidChange(_ session: WCSession) {
        NSLog("%@", "sessionWatchStateDidChange: \(session)")
    }
}
