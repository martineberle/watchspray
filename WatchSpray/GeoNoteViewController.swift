//
//  GeoNoteViewController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 25.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import UIKit

class GeoNoteViewController: UIViewController, WatchSessionConnectable {

    @IBOutlet var connectionStatus: UILabel!
    @IBOutlet var displayLabel: UILabel!

    @IBAction func checkWatchSessionIsReachable(_ sender: UIButton) {
        checkReachability()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.watchSessionManager?.addObserver(self, forKeyPath: "geoNotes", options: [], context: nil)

        checkReachability()
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "geoNotes" {
            OperationQueue.main.addOperation {
                self.updateDisplayLabel(text: self.watchSessionManager?.geoNotes.joined(separator: "\n"))
            }
        }
    }

    deinit {
        self.watchSessionManager?.removeObserver(self, forKeyPath: "geoNotes")
    }

}
