//
//  WatchSessionConnectable.swift
//  WatchSpray
//
//  Created by Martin Eberle on 25.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import UIKit
import AudioToolbox

protocol WatchSessionConnectable {

    var displayLabel: UILabel! { get }
    var watchSessionManager: WatchSessionManager? { get }
    var connectionStatus: UILabel! { get }

    func updateDisplayLabel(text: String?)

}

extension WatchSessionConnectable {

    var watchSessionManager: WatchSessionManager? {
        return (UIApplication.shared.delegate as? AppDelegate)?.watchSessionManager
    }

    func checkReachability() {
        if watchSessionManager?.session.isReachable ?? false {
            connectionStatus.text = "verbunden" // TODO: NSLocalizedString
            connectionStatus.textColor = UIColor.green
        } else {
            connectionStatus.text = "nicht verbunden" // TODO: NSLocalizedString
            connectionStatus.textColor = UIColor.red
        }
    }

    func updateDisplayLabel(text: String?) {
        displayLabel.text = text
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
    
}
