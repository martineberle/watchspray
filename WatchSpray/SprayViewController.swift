//
//  SprayViewController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 17.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import UIKit
import AudioToolbox
import Foundation

class SprayViewController: UIViewController {

    var watchSessionManager: WatchSessionManager!
    var timer: Timer?
    var simulateHighRecyclingRate = false
    var recyclingRate = [Double]() {
        didSet {
            let latestValues = recyclingRate[recyclingRate.indices.suffix(from: max(recyclingRate.startIndex, recyclingRate.endIndex - 10))]

            let sum = latestValues.reduce(0.0) { accumulated, value in
                let recyclingRate1 = value
                let recyclingRate2 = accumulated
                return recyclingRate1 + recyclingRate2
            }

            let flattenedRecyclingRate = String(format: "%.0f", sum / Double(latestValues.count))

            self.recyclingRateValue.text = flattenedRecyclingRate + " %"

            try! watchSessionManager.session.updateApplicationContext(["recyclingRate": flattenedRecyclingRate])
        }
    }

    @IBOutlet var connectionStatus: UILabel!
    @IBOutlet var recyclingRateValue: UILabel!
    @IBOutlet var runStatus: UILabel!
    @IBOutlet var simulateSprayingStatus: UISwitch!
    @IBOutlet var simulateHighRecyclingRateStatus: UISwitch!

    @IBAction func checkWatchSessionIsReachable(_ sender: UIButton) {
        checkReachability()
    }

    @IBAction func simulateSprayingSwitch(_ sender: UISwitch) {
        simulateSpraying()
    }

    @IBAction func highRecyclingRateSwitch(_ sender: UISwitch) {
        simulateHighRecyclingRate = !simulateHighRecyclingRate
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.watchSessionManager = (UIApplication.shared.delegate as? AppDelegate)?.watchSessionManager
        self.watchSessionManager?.addObserver(self, forKeyPath: "sprayRuns", options: [], context: nil)

        checkReachability()
    }

    override func viewWillDisappear(_ animated: Bool) {
        invalidateTimer()
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "sprayRuns" {
            OperationQueue.main.addOperation {
                self.updateMessages()
            }
        }
    }

    func updateMessages() {
        self.runStatus.text = self.watchSessionManager.sprayRuns.last
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }

    func checkReachability() {
        if self.watchSessionManager.session.isReachable {
            self.connectionStatus.text = "verbunden"
            self.connectionStatus.textColor = UIColor.green
        } else {
            self.connectionStatus.text = "nicht verbunden"
            self.connectionStatus.textColor = UIColor.red
        }
    }

    func simulateSpraying() {
        if timer != nil {
            invalidateTimer()
        } else {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
                if self.simulateHighRecyclingRate {
                    self.recyclingRate.append(Double((30...50).random))
                } else {
                    self.recyclingRate.append(Double((15...35).random))
                }
            }
        }
    }

    deinit {
        self.watchSessionManager.removeObserver(self, forKeyPath: "sprayRuns")

        invalidateTimer()
    }

    private func invalidateTimer() {
        timer?.invalidate()
        timer = nil
    }

}

extension CountableRange where Bound: Strideable, Bound == Int {
    var random: Int {
        return lowerBound + Int(arc4random_uniform(UInt32(count)))
    }
    func random(_ n: Int) -> [Int] {
        return (0..<n).map { _ in random }
    }
}
extension CountableClosedRange where Bound: Strideable, Bound == Int {
    var random: Int {
        return lowerBound + Int(arc4random_uniform(UInt32(count)))
    }
    func random(_ n: Int) -> [Int] {
        return (0..<n).map { _ in random }
    }
}
