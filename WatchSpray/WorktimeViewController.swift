//
//  WorktimeViewController.swift
//  WatchSpray
//
//  Created by Martin Eberle on 25.05.17.
//  Copyright © 2017 Martin Eberle. All rights reserved.
//

import UIKit

class WorktimeViewController: UIViewController, WatchSessionConnectable {

    var watchSessionManager: WatchSessionManager!

    @IBOutlet var connectionStatus: UILabel!
    @IBOutlet var displayLabel: UILabel!

    @IBAction func checkWatchSessionIsReachable(_ sender: UIButton) {
        checkReachability()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.watchSessionManager = (UIApplication.shared.delegate as? AppDelegate)?.watchSessionManager
        self.watchSessionManager?.addObserver(self, forKeyPath: "worktimes", options: [], context: nil)

        checkReachability()
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "worktimes" {
            OperationQueue.main.addOperation {
                self.updateDisplayLabel(text: self.watchSessionManager?.worktimes.joined(separator: "\n"))
            }
        }
    }

    deinit {
        self.watchSessionManager?.removeObserver(self, forKeyPath: "worktimes")
    }
    
}
